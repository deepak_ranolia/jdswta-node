const express = require('express');
const {
    getProduct, addProduct, sku, deleteProduct
} = require('../controllers/product');

const router = express.Router();

router.get('/product', getProduct);
router.post('/product', addProduct);
router.post('/sku', sku);
router.post('/delete-product', deleteProduct);

module.exports = router;
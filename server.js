const mongoSanitize = require('express-mongo-sanitize');
const errorHandler = require('./middleware/error');
const bodyParser = require('body-parser');
const connectDB = require('./config/db');
const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const colors = require('colors');
const https = require('https');
const http = require('http');
const path = require('path');
const cors = require('cors');

// Load env vars
dotenv.config({ path: './config/config.env' });

// Connect to database
connectDB();

// Route files
const product = require('./routes/product');

const app = express();
// your EXPRESS configuration here for secured or unsecured http/https connection
// const secured = https.createServer(credentials, app);
const unsecured = http.createServer(app);

app.use(bodyParser.json({ limit: "2mb" }));
app.use(cors());

// Dev logging middleware
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}
// Sanitize data
app.use(mongoSanitize());
// Body parser
app.use(express.json());

// Mount routers
app.use('/api/v1/jd', product);

app.use(errorHandler);

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', express.static('dist/jdswta'))
// Handles any requests that don't match the ones above, will solve reloading issue on any angular route
app.get('/*', (req,res) =>{
  res.sendFile(path.join(__dirname+'/dist/jdswta/index.html'));
})

const PORT = process.env.PORT || 8080;
// unsecured.listen(8080);
// secured.listen(8443);
const server = unsecured.listen(
  PORT,
  console.log(
    `Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold
  )
);

// Handle unhandled promise rejections
process.on('unhandledRejection', (err, promise) => {
  console.log(`Error: ${err.message}`.red);
  // Close server & exit process
  server.close(() => process.exit(1));
});
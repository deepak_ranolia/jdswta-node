const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const switcher = [ "DVD", "Book", "Furniture" ];

var ProductSchema = new Schema({
    sku: {
        type: String,
        required: [true, 'Please, submit required data'],
        unique: true,
        match: [ /^[a-z0-9]+$/i, 'Please, provide the data of indicated type']
    },
    name: {
        type: String,
        required: [true, 'Please, submit required data'],
        match: [ /^[a-z\d\-_\s]+$/i, 'Please, provide the data of indicated type']
    },
    price: {
        type: String,
        required: [true, 'Please, submit required data'],
        match: [ /^\d+(\.\d{2})?$/i, 'Please, provide the data of indicated type']
    },
    productType: {
        type: String,
        enum: switcher,
        required: [true, 'Please, submit required data'],
    },
    size: {
        type: String,
        required: [isRequired('DVD'), 'Please, submit required data'],
        match: [ /^\d+(\.\d{2})?$/i, 'Please, provide the data of indicated type'],
    },
    weight: {
        type: String,
        required: [isRequired('Book'), 'Please, submit required data'],
        match: [ /^\d+(\.\d{2})?$/i, 'Please, provide the data of indicated type'],
    },
    width: {
        type: String,
        required: [isRequired('Furniture'), 'Please, submit required data'],
        match: [ /^\d+(\.\d{2})?$/i, 'Please, provide the data of indicated type'],
    },
    height: {
        type: String,
        required: [isRequired('Furniture'), 'Please, submit required data'],
        match: [ /^\d+(\.\d{2})?$/i, 'Please, provide the data of indicated type'],
    },
    length: {
        type: String,
        required: [isRequired('Furniture'), 'Please, submit required data'],
        match: [ /^\d+(\.\d{2})?$/i, 'Please, provide the data of indicated type'],
    },
});

function isRequired(val) {
    if ( val.indexOf(this.productType) > -1 ) {
        return true;
    }
    return false;
}

module.exports = mongoose.model('Product', ProductSchema);
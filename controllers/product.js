// const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Product = require('../models/Product');

// @desc      get product
// @route     get /api/v1/jd/getProduct
// @access    Public
exports.getProduct = asyncHandler(async (req, res, next) => {
  const product = await Product.find().select('-__v')
  res.status(200).json({
    a: 'getProduct', v: 'v1', s: 1,
    l: '',
    n: '',
    f: product,
    d: product,
  });
});

// @desc      find SKU
// @route     get /api/v1/jd/sku
// @access    Public
exports.sku = asyncHandler(async (req, res, next) => {
  const product = await Product.findOne(req.body)
  if ( !product ) {
    res.status(200).json({
      a: 'sku', v: 'v1', s: 1, l: '', n: 'product',
      f: product, d: product,
    });
    return
  }
  res.status(401).json({
    error: `SKU: ${req.body.sku} already exists in product list`
  });
});

// @desc      add product
// @route     get /api/v1/jd/addProduct
// @access    Public
exports.addProduct = asyncHandler(async (req, res, next) => {
  const product = await Product.create( req.body)
  res.status(200).json({
    a: 'addProduct', v: 'v1', s: 1,
    l: '',
    n: '',
    f: product,
    d: product,
  });
});

// @desc      delete product/s
// @route     get /api/v1/jd/deleteProduct
// @access    Public
exports.deleteProduct = asyncHandler(async (req, res, next) => {
  const { arr } = req.body
  const product1 = await Product.deleteMany({ _id : {$in: arr} });
  const product2 = await Product.find().select('-__v')
  res.status(200).json({
    a: 'deleteProduct', v: 'v1', s: 1,
    l: '',
    n: '',
    f: product1,
    d: product2,
  });
});